import pytest
from aiohttp import web

from app.router import setup_routes


@pytest.fixture
async def app(loop):
    app = web.Application()
    setup_routes(app)

    return app


@pytest.fixture
async def client(app, aiohttp_client):
    client = await aiohttp_client(app)
    return client
